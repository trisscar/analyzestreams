\documentclass[conference]{IEEEtran}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{patterns}
\newcommand\myworries[1]{\textcolor{red}{#1}}
\usepackage{listings}
\definecolor{ffirst}{HTML}{FC8D59}
\definecolor{ssecond}{HTML}{FFFFBF}
\definecolor{tthird}{HTML}{0D0D0D}
\definecolor{bblack}{HTML}{000000}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Program Analysis to Discover Strided Access Patterns}

\author{\IEEEauthorblockN{William Galliher}
\IEEEauthorblockA{\textit{Computer Science dept. University of Wisconsin (Madison)} \\
\textit{University of Wisconsin, Madison}\\
Madison, Wisconsin, USA \\
galliher@wisc.edu}
}

\maketitle

\begin{abstract}
Memory accesses within loops are areas of particular interest for acceleration, with many microarchitecture tools like prefetchers and accelerators aiming to exploit strided access patterns to speed up programs. However, finding patterns is often difficult, and there is a lack of easy to use tools to assist programmers in this effort.

To help reduce this effort, we propose an analysis tool to assist in identifying candidate memory accesses and expressions that form the pattern. Using these tools will point the programmer towards useful candidate areas, and can accelerate transforming programs to exploit microarchitectural features.
\end{abstract}

\begin{IEEEkeywords}
Program, Analysis, Memory, Patterns, Accelerators, Compilation, Tools
\end{IEEEkeywords}

\section{Introduction} \label{introduction}
In the age of powerful multi-core processors and new accelerators, memory continues to be a main focus for many programs. Programs such as image processing, neural nets, database operations, scientific computing, and cryptocurrency often work over large data sets, incorporating loops that consume these datasets to do different operations.

To accelerate these programs, there are microarchitectural features and accelerators that can exploit strided access patterns to speed up memory access. These include memory prefetch engines that can either be hinted to prefetch from memory, or attempt to determine what to prefetch without any input from the programmer, and accelerators such as Stream Dataflow accelerators. Stream Dataflow accelerators are accelerators that can perform computation kernels that are fed all of their data using memory ``streams'', which describe a strided access pattern and are consumed by specialized hardware to access memory in a fast and efficient manner \cite{streamdataflow}.


Consider the dot product example given in listing \ref{dotp-example}.
\begin{lstlisting}[caption={Dot Product Example},
    captionpos=b,
    label={dotp-example},
    float,
    language=C++]
#include <stdio.h>
#include <inttypes.h>

#define ALEN 256

uint16_t array1[ALEN]={1};
uint16_t array2[ALEN]={1};

uint64_t out = 0;

int main(int argc, char* argv[]) {
  for(int i = 0; i < ALEN; ++i) {
    array1[i]=1;
    array2[i]=1;
  }

  uint16_t sum0=0;
  uint64_t sum1=0, sum2=0;

  for(int i = 0; i < ALEN; ++i) {
    sum0 += array1[i] * array2[i];
  }
  printf(``dot product (original): %d\n'',sum0);
}
\end{lstlisting}

This program has two loops, and in both loops, the two arrays (\verb|array1| and \verb|array2|), are accessed in a linear manner based on the loop variable \verb|i|, which increments by 1 every iteration of the loop. Such a pattern can be exploited to prefetch or accelerate these memory accesses.

However, knowing these patterns are good and rewriting existing programs to make use of these patterns are two different things. Taking an existing program and determining which memory accesses exhibit strided access patterns can often be a confusing and high effort task, particularly if the programmer is not experienced in looking for these patterns. To help with this, we propose a way of identifying memory accesses at compile time, that reports both different memory accesses inside of loops and details if the access fits a strided access pattern over all iterations of the loop, and what expressions in the loop are factored into that decision. We further implement this algorithm into a popular compiler tool, enabling a pattern report to be given at compile time to the programmer to jump start their efforts in optimizing their memory access.

\section{Pattern Discovery Algorithm} \label{pattern-discover-algorithm}
\subsection{Basics and Assumptions} \label{basics-and-assumptions}
When determining which patterns we are attempting to detect, we focused on strided access patterns, the pattern used in Stream Dataflow accelerators. Strided access patterns refer to a constant access size and stride size where the stride amount is either equal to the amount of iterations for the loop, or a high percentage of the iterations of the loop. Such strided access patterns are good candidates for programmers to exploit for accelerating memory accesses.

Strided access patterns themselves are patterns of memory access described using three numbers:
\begin{itemize}
\item \textbf{Access size:} This is the size of the memory access, generally given in bytes.
\item \textbf{Stride size:} This is the amount the address for the memory access is modified for the next access, generally given in bytes.
\item \textbf{Stride amount:} This is the amount of times a particular constant access size and stride size is repeated.
\end{itemize}

Therefore, to determine if a particular memory access fits a strided access pattern, we want to determine that, for every iteration of a loop, the access size is constant and the stride size is constant. If this holds for every iteration of the loop, then the stride amount is then the amount of iterations of the loop. Note, we do not need to know the actual value of the access size, stride size, or stride amount, we merely just need to determine if they are constant at the point in the program where we are executing the loop.

Now that we have determined what a strided access pattern is, we will detail how we build a report for a program on which memory accesses hold to a strided access pattern. To do this, we will have four major parts to consider.
\begin{enumerate}
\item \textbf{\(L\)}: The loops present inside the program.
\item \textbf{\(A\)}: The memory accesses within a given loop \(l \in L\).
\item \textbf{\(V\)}: All variables which are used in the index expression for memory access \(a \in A\) or are used in an expression that modifies another variable \(v \in V\).
\item \textbf{\(X\)}:The value of the index expression for a given iteration \(i\) in the loop \(l \in L\).
\end{enumerate}

Since we are analyzing memory accesses in the context of a loop, the iterations of the loop itself correspond to the stride amount. We can also state that the access size will be constant for each memory access if the language the program is written in encodes the access size into the access itself, as most instruction set architectures (such as x86 and RISCV) do. Then for a memory access to hold to a strided access pattern, given the previous assumptions, the last remaining thing we must determine is if the stride size is constant. This can be written in terms of the former four groups as such:

\emph{For every \(a \in A\), \(l \in L\), \(x \in X\) in program P, access a holds to a strided access pattern if, for loop iteration i, \(x_i = x_{i-1} + C\), where C is a constant.}

Our primary goal is to identify if the relationship \(x_i = x_{i-1} + C\), which we will call the \emph{stride size relationship}, holds true. To do that, we propose an algorithm that walks the abstract syntax tree for the program P, and determines if the index expression's value \(x\) for each index \(i\) holds to this relationship. Note, we do not need to know the exact value of any \(x\) or for the constant \(C\), just that the relationship between different iterations holds. This then fills out the last remaining part of the strided access pattern. The \textbf{access size} is the size of the variable type the memory access is composed of, the \textbf{stride size} is the constant \(C\), and the \textbf{stride amount} is the amount of iterations in the loop.

\subsection{Evaluating Expressions using the Stride Size Relationship} \label{evaluating-expressions}

The final piece we need to discuss before getting into the algorithm itself is how we determine if an index holds to the stride size relationship, and to answer that, we need to be able to evaluate expressions themselves to determine if they hold. To help with classification in this manner, we set forward three abstract states that expressions can be in related to the stride size relationship. There are \textbf{locally constant}, \textbf{strided}, and \textbf{unsure}. All expressions we consider start in the \textbf{locally constant} state, and depending on the expressions in their subtree, move up the abstract lattice. Moving up in the lattice is then determined by the value of the constant in the stride size relationship. If the constant is 0, then the expression is locally constant, as the value of each iteration \(x_i\) is equal to the previous \(x_{i-1}\). If the value of the constant \(C\) is equal to some non-zero constant value, than the state is strided, as the index for each iteration moves forward by some constant \(C\). Finally, if the constant \(C\) cannot be determined actually be constant, then we enter the unsure state, which is equivalent to top in the abstract lattice. 

To illustrate what is happening, consider the array access in listing \ref{array-example}.
\begin{lstlisting}[caption={Simple Array Access},
    captionpos=b,
    label={array-example},
    float,
    language=C++]
  array[i*row + j] = 1;
\end{lstlisting}

The expression we might be interested in is the index expression used in the access to \verb|array|, \verb|i*row + j|. There are two expressions that are operations, \verb|*| and \verb|+|, and three expressions that are variable accesses, \verb|i|, \verb|row|, and \verb|j|. Everything will start in the initial \textbf{locally constant} state. Then we evaluate the two operations.
For \verb|*|, we can plug in an example, \(A * B\) into the stride size relationship to see which conditions allow it to hold, where \(A\) and \(B\) are the child expressions of the \(*\) expression. Plugging this in to the stride size relationship is shown in equation \ref{multiply-relationship}. We will evaluate this equation to determine the state transition table for the the expression * based off of the states of its children expressions, A and B. We won't show examples for if A or B are already in a \textbf{unsure} state, as if either are already unsure, an expression cannot result in a state that is no longer \textbf{unsure}. The expression state has already reached the highest abstract state (T).
\begin{flalign}
(A_i * B_i) = (A_{i-1} * B_{i-1}) + C
\label{multiply-relationship}
\end{flalign}

We can then evaluate this in the context of the different possible states for the sub expressions A and B. If A and B are both \textbf{locally constant}, then \(A_i * B_i\) is equal to \(A_{i-1} * B_{i-1}\), and the constant stride size C is equal to 0, meaning the state of the expression for * is also \textbf{locally constant}.

Another situation is if  one of the expressions A or B is \textbf{strided}, and the other is \textbf{locally constant}. If A, for example, is locally constant, than \(A_i = A_{i-1}\). If B is strided, then we know that \(B_{i-1} = B_i - C_B\), where \(C_B\) is the constant stride size for the expression B. Therefore, the relationship in equation \ref{multiply-relationship} can be rewritten as shown in equations \ref{mult-constant-strided-begin} to \ref{mult-constant-strided-end}. The the final result is \(A_i * C_B = C\). Since \(A_i\) and \(C_B\) are both constant, the value of C itself is constant, so the state of the * expression is \textbf{strided}.
\begin{flalign}
(A_i * B_i) = (A_i * (B_i - C_B)) + C \label{mult-constant-strided-begin}\\
(A_i * B_i) = A_i * B_i - A_i * C_B + C \\
A_i * B_i - A_i * B_i + A_i * C_B = C \\
A_i * C_B = C
\label{mult-constant-strided-end}
\end{flalign}

The final combination of states is if both A and B are \textbf{strided}. This means that \(A_{i-1} = A_i - C_A\), where \(C_A\) is the constant stride size for the expression A, and \(B_{i-1} = B_i - C_B\), where \(C_B\) is the constant stride size for the expression B. We can plug these into equation \ref{multiply-relationship} and evaluate the result as shown in equations \ref{mult-stride-stride-begin} to \ref{mult-stride-stride-end}. The resulting left side assignment to C is not constant, as it includes the strided values \(A_i\) and \(B_i\), so the resulting state for the * expression is \textbf{unsure}
\begin{flalign}
(A_i * B_i) = ((A_i - C_A) * (B_i - C_B)) + C \label{mult-stride-stride-begin}\\
(A_i * B_i) = A_i * B_i - C_A * B_i - C_B * A_i + C_A * C_B + C \\
A_i * B_i - A_i * B_i + A_i * C_B + B_i * C_A - C_A * C_B = C \\
A_i * C_B + B_i * C_A - C_A * C_B = C
\label{mult-stride-stride-end}
\end{flalign}

After evaluating all of these equations, the state transformation table for the * operation is shown in table \ref{mult-state-table}.
\begin{table}[htbp]
\begin{center}
\begin{tabular}{ c | c | c | c }
 *               & Locally Constant          & Strided          & Unsure \\
\hline Locally Constant & \textbf{Locally Constant} & \textbf{Strided} & \textbf{Unsure} \\
\hline Strided          & \textbf{Strided}          & \textbf{Unsure}  & \textbf{Unsure} \\
\hline Unsure           & \textbf{Unsure}           & \textbf{Unsure}  & \textbf{Unsure}
\end{tabular}
\end{center}
\caption{Multiply State Transition Table}
\label{mult-state-table}
\end{table}

Similar exercises have been done for addition operations. If there is an expression that we do not recognize, such as a function call where we cannot walk the function call's tree or other user defined operation, the state transition table will always result in \textbf{unsure}, as unsure is the top abstract state and the safe state when we cannot say with confidence that an expression results in a locally constant or strided value.

\subsection{Algorithm} \label{algorithm}
We have laid forth the groundwork to begin evaluating the abstract syntax tree to discover the strided access patterns, now we will show the algorithm.  The algorithm for determining if the stride size relationship holds true can be done as a working list algorithm that works on a working list of variables expressions that need to be checked to determine if they will hold to a constant stride size, or not. Therefore, our algorithm works by adding things to a working list of variables that need to be analyzed, and repeatedly walking the abstract syntax tree until the working list is empty. The four main steps are:
\begin{enumerate}
\item \textbf{Find loops}
\item \textbf{Find memory accesses indexes}
\item \textbf{Traverse Loop with Working List}
\item \textbf{Determine Pattern for Memory Access}
\end{enumerate}

\subsection{Find Loops} \label{find-loops}
Finding loops is the most trivial part of the algorithm. We simply walk the abstract syntax tree for the program, and for every loop expression, we take the entire subtree with that loop expression as its root. The rest of the algorithm is then run on that subtree. After the rest of the algorithm is complete, we repeat on any other loop expressions we find, until we have exhausted walking the entire program's tree.

One small question to address is nested loops. As we will see when discussing the other parts of the algorithm, nested loops can be useful to analyze as if no special treatment is given. That is, consider two nested for loops, like shown in listing \ref{nested-for-loop}. If we simply walk the abstract syntax tree, we will find two subtrees. One with the outer loop, which includes the inner loop, and one with just the inner loop. Each of these can be evaluated separately, and some value can be gathered on whether a memory access index holds to the stride size relationship with just the inner loop scope, or with the outer loop scope.
\begin{lstlisting}[caption={Nested For Loops},
    label={nested-for-loop},
    captionpos=b,
    float,
    language=C++]
  for (int i = 0; i < row; i++) {
    for (int j = 0; j < col; j++) {
      array[i*row + j] = 1;
    }
  }
\end{lstlisting}


\subsection{Find Memory Acccess Indexes} \label{find-accesses}
 For each memory access, we will have two parts that are important to consider, the \textbf{base}, the \textbf{index expression}. The base expression serves to describe the access size through whatever the size of the resultant type of the expression is. The index expression serves two purposes. One, all variable expressions inside the index expression are used to seed the initial working list, all of them starting at state \textbf{locally constant}. The index expression itself is also included in the working list, as the state of the index expression will determine whether the memory access has a constant, strided, or unsure access pattern.
Consider a memory access as an array expression, as shown in list \ref{array-example}. The base expression is \verb|array|, and the index expression is \verb|i*row + j|.  The initial contents of the working list are then \verb|i|, \verb|row|, \verb|j|, and the root of the index expression tree, the expression \verb|+|.

\subsection{Traverse Loop with Working List} \label{traverse-loop}
Now that we have a seeded working list, we take the loop's tree and walk it. For every expression in the tree that modifies a variable expression in the working list, we evaluate the modifying expression using the methodology in section \ref{evaluating-expressions}. For each variable expression inside the new expression we are evaluating, we will insert those expressions on to the working list. Eventually, we will have the resulting state from evaluating the modifying expression, and will need to determine what the new state is for the variable that the expression modifies.

To determine this new state for the variable, we need to consider the state of the variable \verb|v| and the resulting state from the state transition for the modifying expression \verb|e|. This is shown in table \ref{modifying-state-table}.
\begin{table}[htbp]
\begin{center}
\begin{tabular}{ c | c | c | c }
                 & \verb|e|: Locally Constant          & \verb|e|: Strided          & \verb|e|: Unsure \\
\hline \verb|v|: Locally Constant & \textbf{Locally Constant} & \textbf{Strided} & \textbf{Unsure} \\
\hline \verb|v|: Strided          & \textbf{Strided}          & \textbf{Strided}  & \textbf{Unsure} \\
\hline \verb|v|: Unsure           & \textbf{Unsure}           & \textbf{Unsure}  & \textbf{Unsure}
\end{tabular}
\end{center}
\caption{Resulting State for Variable v}
\label{modifying-state-table}
\end{table}

Two parts of note are when the state of \verb|v| is strided and when the state transition from \(e\) is locally constant. The reason the resulting state is kept as strided is that the algorithm does not distinguish if the modifying expression comes before or after the memory access. Because of this, we cannot know if the memory access index should treat the state of the variable as strided or locally constant. Since strided is higher in the abstract lattice, we take the higher of the two states and assign the state of the variable as \textbf{strided}. The other is when they both are strided. In both cases, we can know that before the modifying expression, the variable had a strided state, and after the modifying state, it may have a different constant, but is still in a strided state. Therefore, a memory access before or after the expression can continue to treat the state of the variable as strided.

After evaluating the new state for the variable \(v\), if the new state is different than the old state, the variable \(v\) is inserted back on to the working list.

\subsection{Determine Pattern for Memory Access} \label{determining-pattern}

The working list is now empty, so we have the states for all variables and the index expression. The access pattern for the memory access is then based on the state for the index expression. For all cases, the memory access has a pattern where the access size is determined by the base expression, and the stride amount is the iterations of the loop.
\begin{itemize}
\item \textbf{Locally Constant:} The stride size is zero.
\item \textbf{Strided:} The stride size is constant.
\item \textbf{Unsure:} The stride size cannot be determined to be constant or zero.
\end{itemize}

We say we are unsure because this algorithm does not prove that a particular access pattern is \emph{not} strided, so it is possible that it is strided or constant, but we are unable to determine this at compile time, either because of an expression we do not recognize that modifies the index, or a modifying expression has the chance of resulting in pattern that is not strided or constant.

We can then repeat the algorithm for every loop inside of the program to build a comprehensive report on the patterns for all memory accesses within all loops.
\section{Methodology} \label{methodology}
\subsection{Tools and Reporting} \label{tools}
We implemented our algorithm as a plugin in Clang to assist in identifying these patterns. We chose Clang because it is a robust compiler frontend for llvm and offers a robust library for writing abstract syntax tree traversers \cite{clang}. Since the frontend supports a number of languages that will be converted to the internal abstract syntaxt tree representation, the same plugin can be used without extensive rewriting necessary for different language support. In addition, the Clang development team has written many examples and extensive documentation for using their abstract syntax tree representation, further reducing the effort of implementation for our plugin. The plugin can then be run on any source code that Clang can compile at the compilation step by including the compiled plugin and setting a flag on the command line.

Each step of the algorithm is implemented in Clang as a recursive abstract syntax tree traverser. The main function consumes the root of the abstract syntax tree for the program and begins the first step of the algorithm, discovering loops. Currently, our implementation supports for and while loops. Then, we discover memory accesses in the form of array accesses, and begin the working list. Expressions we support for evaluation are the unary increment and decrement, the addition, subtraction, multiplication, division, and shift binary operations, and detecting assignment using unary operations and both assignment and compound assignment. Once we have finished the algorithm, we report each memory access's expression, the resulting state, and the list of expressions that affect this determination. All of these expressions are reported in the format of the original source code, making it easy for the programmer to correlate what was included in the determination to what they have written. All the code for the plugin itself (as well as this paper), can be found at https://gitlab.com/trisscar/analyzestreams.

\subsection{Test Cases} \label{test-cases-section}
To evaluate the quality of our tool, we chose a number of tests that had already been evaluated for use in Stream Dataflow accelerators. We therefore know that memory accesses exist in these programs that follow strided access patterns to judge if our tool is finding these worthwhile access patterns. In addition, we can report the number of loops in the program, the number of memory accesses, and for all of these memory accesses, how many were determined to be constant, strided, and unsure. All the test cases, with the exception of the simple dot product program \verb|dotp|, are from MachSuite, a suite that tests different kernels that may want to be run on accelerators. The information about each test is taken from the MachSuite paper\cite{machsuite}, and summarized in table \ref{test-cases}.
\begin{table}[htbp]
\begin{center}
\begin{tabular}{ p{1.5cm} | p{5.5cm} }
 Test Name & Behavior \\
\hline stencil2d & Image convolution on a 2D image \\
\hline stencil3d & Image convolution on a 3D image \\
\hline aes & Advance Encription Standard block cypher \\
\hline backprop & Neural network training \\
\hline bfs/queue & ``Expanding horizon'' breadth-first search \\
\hline fft/transpose & Two-level FFT optimized for small, fixed-size butterfly \\
\hline gemm/blocked & A blocked version of matrix multiplication \\
\hline kmp & Knuth-Morris-Pratt string matching algorithm \\
\hline md/knn & n-body molecular dynamics, using k-nearest neighbors \\
\hline nw & Dynamic programming algorithm for optimal sequence alignment \\
\hline sort/radix & Sorts index array by comparing 4 bit blocks \\
\hline dotp & A simple dot product on two vectors
\end{tabular}
\end{center}
\caption{Test Cases}
\label{test-cases}
\end{table}

\section{Results} \label{results-section}
\begin{figure*}[!t]
  \centering
\begin{tikzpicture}
  \begin{axis}[
      width  = \textwidth,
      height = 6cm,
      major x tick style = transparent,
      ybar=2*\pgflinewidth,
      bar width=11pt,
      ymajorgrids = true,
      ylabel = {Memory Accesses},
      symbolic x coords={stencil2d, stencil3d, aes, backprop, bfs/queue, fft/transpose, gemm/blocked, kmp, md/knn, nw, sort/radix},
      xtick=data,
      ymin=0,
      x tick label style={rotate=45,anchor=east}
    ]
    \addplot[style=bblack,fill=ffirst,mark=none] coordinates {(stencil2d,6) (stencil3d,6) (aes,6) (backprop,6) (bfs/queue,6) (fft/transpose,6) (gemm/blocked,6) (kmp,6) (md/knn,6) (nw,6) (sort/radix,6)};
    \addplot[style=bblack,fill=ssecond,mark=none] coordinates {(stencil2d,4) (stencil3d,6) (aes,4) (backprop,15) (bfs/queue,15) (fft/transpose,15) (gemm/blocked,4) (kmp,4) (md/knn,10) (nw,4) (sort/radix,8)};
    \addplot[style=bblack,fill=tthird,mark=none] coordinates {(stencil2d,12) (stencil3d,12) (aes,12) (backprop,12) (bfs/queue,12) (fft/transpose,12) (gemm/blocked,12) (kmp,12) (md/knn,12) (nw,12) (sort/radix,12)};
    \legend{\strut Constant, \strut Strided, \strut Unsure}
  \end{axis}
\end{tikzpicture}
\caption{Analysis Results from Compiling MachSuite}
\label{results}
\hrulefill
\vspace*{4pt}
\end{figure*}
The count of each access for each test that held to constant, strided, and unsure access patterns are shown in figure \ref{results}. Since almost all of these tests come from the same suite, most share a very similar structure of a one, two, or three nested loop as the main function of the test, so the results are mostly the same. The loops that were originally converted to Stream Dataflow were detected as strided, for the tests that had been converted to Stream Dataflow before (stencil2d, dotp). However, there are still quite a few accesses labeled as unsure. Upon examination of the abstract syntax tree for the loop and the underlying code, most of that had to do with an enhancement that could be made to how we handle variables. Our current implementation only takes variables that, at their root, are only \verb|DeclRefExpr| types, which is the base type for a reference to a declaration in Clang's abstract syntax tree representation. We do not recursively search down if the variable is a pointer access and track those as well. A simple example of this is shown in listing \ref{complicated-variables}. If we have more generalized expressions that could include references like \verb|struct->iter| as well, we should be able to capture these behaviors too.

\begin{lstlisting}[caption={More Complicated Variables},
    captionpos=b,
    label={complicated-variables},
    float,
    language=C++]
  i = struct->iter;
  array[i] = 1
  struct->iter++;
\end{lstlisting}

Finally, we chose MachSuite because a couple tests had already been converted to Stream Dataflow kernels. However, the different types of accesses in MachSuite proved to be very similar, which menas our results across different tests felt much the same. We begin work to try and test another suite, but ran into other issues compiling before the work could be completed. The tool would benefit from being run on more tests and steadily widened in what it can support.
\section{Future Work and Considerations} \label{future-work}
Our algorithm could be expanded upon in a few ways. First, we could identify expressions in the working list that are before or after the memory access, and make some choices on what state the index expression should be in based on if the modifying expression comes before or after the index expression. Second, we could add some partial evaluation stages, where we set the values of some unsure variables and see if information gathered could move some states from being unsure to being strided.

The implementation is also limited by the amount of operations we recognize. We can continue to build on this tool by implementing more state transition functions for new operations, widening the amount of expressions we can evaluate. We can also emite all of the expressions into other special domain specific languages for tools such as Petit and the Omega Calculator \cite{omegacalculator}. These tools can be used for evaluating dependence and other constraints on more complicated loops, and just require exporting to their specific parsing language. Since Clang supports rewriting and code export tools, they could be built into our implementation to further the richness of our evaluation.

Finally, this work was written with the goal to work towards rewriting such memory accesses into the ``streams'' used in Stream Dataflow accelerators, as well as rewriting expressions that use this data into a dataflow ``kernel''. Such a tool would use the plugin we have developed here to automatically rewrite loops to be used for Stream Dataflow accelerators.

\section{Related Work} \label{reulated-work}
Discovering strided access patterns shares a similar goal to other loop analysis tools. One that is present and used often in most compilers is automatic vectorization. The goal of such tools is to determine if an operation that is being done inside a loop can be unrolled and converted into a single operation that performs many iterations of the operation in one. For example, if a 32 bit add is being done between an array of 32 bit datatypes, a vectorizer may see if it can combine 8 of these operations and do an 8 wide 32 bit add in a single instruction. Determining this involves analyzing the pattern as well, where the access size and the stride size are equal, resulting in a linear traversal of the memory for the operands. This only differs in that strided access patterns have a more loose requirement, as the access size and stride size do not have to be equal. Perhaps even using machine learning to enhance our ability to detect some patterns when performing some partial evaluation, as it is used to assist in automatic vectorization as well \cite{mlvectorization}.

Another similar work are the algorithms used in strided prefetchers\cite{stridedprefetcher}. These algorithms lack the context that compilation and the abstract syntax tree provide, but the action that they take is less impactful than rewriting code. They instead aim to grab memory accesses ahead of time, exploiting the cache structure in modern processors to avoid the latency of going to main memory. These algorithms could be useful in a tool like ours, when used with partial evaluation techniques and evaluating the loop and memory access expressions with a range of fixed values. The resulting index values from setting the value of some variables and leaving others as dynamic could move some states from unsure to strided, especially with operations we do not know how to evaluate.

\begin{thebibliography}{00}
\bibitem{omegacalculator} Beletskyy, Volodymyr, R. Drakowski, and Marcin Liersz. ``An approach to parallelizing non-uniform loops with the omega calculator.'' In Proceedings. International Conference on Parallel Computing in Electrical Engineering, pp. 119-122. IEEE, 2002.
\bibitem{stridedprefetcher} Kondguli, Sushant, and Michael Huang. ``T2: A highly accurate and energy efficient stride prefetcher.'' In 2017 IEEE International Conference on Computer Design (ICCD), pp. 373-376. IEEE, 2017.
\bibitem{clang} Lattner, Chris. ``LLVM and Clang: Next generation compiler technology.'' In The BSD conference, vol. 5. 2008. Project URL https://llvm.org/
\bibitem{streamdataflow} Nowatzki, Tony, Vinay Gangadhar, Newsha Ardalani, and Karthikeyan Sankaralingam. ``Stream-dataflow acceleration.'' In 2017 ACM/IEEE 44th Annual International Symposium on Computer Architecture (ISCA), pp. 416-429. IEEE, 2017.
\bibitem{machsuite} Reagen, Brandon, Robert Adolf, Yakun Sophia Shao, Gu-Yeon Wei, and David Brooks. ``Machsuite: Benchmarks for accelerator design and customized architectures.'' In 2014 IEEE International Symposium on Workload Characterization (IISWC), pp. 110-119. IEEE, 2014.
\bibitem{mlvectorization} Stock, Kevin, Louis-Noël Pouchet, and P. Sadayappan. ``Using machine learning to improve automatic vectorization.'' ACM Transactions on Architecture and Code Optimization (TACO) 8, no. 4 (2012): 50.
\end{thebibliography}
\end{document}
