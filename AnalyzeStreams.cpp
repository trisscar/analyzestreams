//===- PrintFunctionNames.cpp ---------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Example clang plugin which simply prints the names of all the top-level decls
// in the input file.
//
//===----------------------------------------------------------------------===//

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Sema/Sema.h"
#include "llvm/Support/raw_ostream.h"
#include <vector>
using namespace clang;

namespace {
#define LOCALLY_CONSTANT 0
#define STRIDED 1
#define UNSURE 2

class AnalyzeStreamsConsumer : public ASTConsumer {
  CompilerInstance &Instance;
  std::set<std::string> ParsedTemplates;
  int loops_analyzed = 0;
  int array_accesses_analyzed = 0;
  int total_locally_constant = 0;
  int total_strided = 0;
  int total_unsure = 0;

public:
  AnalyzeStreamsConsumer(CompilerInstance &Instance,
                         std::set<std::string> ParsedTemplates)
      : Instance(Instance), ParsedTemplates(ParsedTemplates) {}

  int is_safe(Stmt* S, int state) {
    if (UnaryOperator* UO = dyn_cast<UnaryOperator>(S)) {
      if (UO->isIncrementOp()
	  || UO->isDecrementOp()
	  || UO->isIncrementDecrementOp()) {
	return STRIDED;
      } else {
	// Currently don't know, so return false
	return UNSURE;
      }
    } else if (BinaryOperator* BO = dyn_cast<BinaryOperator>(S))  {
      // Check binary ops here.
      if (BO->isAdditiveOp() || BO->isMultiplicativeOp() ||
	  BO->isShiftOp())
	return STRIDED;
      else
	return UNSURE;
    } else {
      // Not an operator we recognize, not an assignment
      return state;
    }
  }

  std::string assignsToVariable(Stmt* S, std::map<std::string, int>* analyzed_variables) {
    // Recursively go down on children.
    if (S == NULL) {
      return "";
    }
    if (ArraySubscriptExpr *ASE = dyn_cast<ArraySubscriptExpr>(S)) {
      // Stupidly return with a warning
      auto loopRange = CharSourceRange::getTokenRange(ASE->getBeginLoc(), ASE->getEndLoc());
      auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
      //llvm::errs() << "Assuming array access will not be a variable access: " << ref.str() << "\n";
      return "";
    }
    if (DeclRefExpr *DRE = dyn_cast<DeclRefExpr>(S)) {
      auto loopRange = CharSourceRange::getTokenRange(DRE->getBeginLoc(), DRE->getEndLoc());
      auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
      if (analyzed_variables->find(ref.str()) != analyzed_variables->end()) {
	// This does reference the variable we care about
	return ref.str();
      }
    }
    std::string variable = "";
    if (S->child_begin() != S->child_end()) {
      // We have children. Recursively go down.
      for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
	if (variable == "")
	  variable = assignsToVariable(*child, analyzed_variables);
      }
    }
    return variable;
  }

  std::string handleArrayAccesses(Stmt* S) {
    std::string return_string = "";
    if (MemberExpr *ME = dyn_cast<MemberExpr>(S)) {
      // Pointer member, include in string.
      auto loopRange = CharSourceRange::getTokenRange(ME->getBeginLoc(), ME->getEndLoc());
      auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
      return ref.str();
    }
    if (DeclRefExpr* DRE = dyn_cast<DeclRefExpr>(S)) {
      auto loopRange = CharSourceRange::getTokenRange(DRE->getBeginLoc(), DRE->getEndLoc());
      auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
      return ref.str();
    }
	
    for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
      return_string = handleArrayAccesses(*child).append(return_string);
    }
    return return_string;
  }

  int analyzeVariables(Stmt* S, bool* keep_analyzing, int current_state, bool in_assignment,
			std::map<std::string, int>* analyzed_variables) {
    // Recursively go down on children.
    if (S == NULL) {
      return current_state;
    }
    if (DeclRefExpr *DRE = dyn_cast<DeclRefExpr>(S)) {
      // We have a reference access!
      if (in_assignment) {
	// Is this variable safe / constant? 0 means constant (no modifications), 1 means stream safe modifications
	// 2 is top and means unsafe.
	auto loopRange = CharSourceRange::getTokenRange(DRE->getBeginLoc(), DRE->getEndLoc());
	auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	if (analyzed_variables->find(ref.str()) == analyzed_variables->end()) {
	  // We have a variable that we need to check.
	  *keep_analyzing = true;
	  (*analyzed_variables)[ref.str()] = 0;
	}
	return (*analyzed_variables)[ref.str()] != 2;
      }
    }
    // Check safety of this operation, if it is an operation that can be checked simply
    int checked_state = is_safe(S, current_state);

    // Handle binary operation specifics
    if (BinaryOperator *BE = dyn_cast<BinaryOperator>(S)) {
      if (BE->isAssignmentOp() || BE->isCompoundAssignmentOp()) {
	// Check to see if we care about this assignment (namely, does it assign to a variable we care about)
	std::string variable = assignsToVariable(BE->getLHS(), analyzed_variables);
	if (variable != "") {
	  // We care about the right side now...
	  // First, check to see if it is a constant
	  Expr::EvalResult result;
	  if (BE->getRHS()->EvaluateAsInt(result, Instance.getASTContext())) {
	    // Sweet! It is a constant!
	    // Just check if we should warn if we _weren't_ constant
	    if (analyzed_variables->find(variable) != analyzed_variables->end()) {
	      auto loopRange = CharSourceRange::getTokenRange(BE->getRHS()->getBeginLoc(), BE->getRHS()->getEndLoc());
	      auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	      llvm::errs() << ref.str() << ": Var: " << variable << " Val: " << (*analyzed_variables)[variable] << "\n";
	      if ((*analyzed_variables)[variable] > 0) {
		llvm::errs() << "We are overwriting a potential non-safe with a constant.\n";
		exit(1);
	      }
	    } else {
	      *keep_analyzing = true;
	    }
	    (*analyzed_variables)[variable] = 0;
	    return current_state;
	  }
	  int state = 0;
	  for (StmtIterator child = BE->getRHS()->child_begin(), child_end = BE->getRHS()->child_end();
	       child != child_end; ++child) {
	    int newstate = analyzeVariables(*child, keep_analyzing, (checked_state > current_state) ? checked_state : current_state,
					    in_assignment, analyzed_variables);
	    state  = (newstate > state) ? newstate : state;
	  }
	  if (analyzed_variables->find(variable) == analyzed_variables->end()) {
	    *keep_analyzing = true;
	  } else {
	    if (((state != 2) && (*analyzed_variables)[variable] != 1)
		|| ((state == 2) && (*analyzed_variables)[variable] != 2)) {
	      *keep_analyzing = true;
	    }
	  }
	  (*analyzed_variables)[variable] = state;
	  return current_state;
	}
      }
      // Not an assignment
      if ((BE->isMultiplicativeOp() || BE->isShiftOp() || BE->isAdditiveOp()) && in_assignment) {
	// We care about the right side now...
	// First, check to see if it is a constant
	Expr::EvalResult result;
	int lhs, rhs;
	lhs = -1;
	rhs = -1;
	if (BE->getRHS()->EvaluateAsInt(result, Instance.getASTContext())) {
	  // Sweet! It is a constant!
	  rhs = 0;
	}
	if (BE->getLHS()->EvaluateAsInt(result, Instance.getASTContext())) {
	  // Sweet! It is a constant!
	  lhs = 0;
	}
	// Get the LHS and RHS values if not already set
	if (lhs) {
	  lhs = analyzeVariables(BE->getLHS(), keep_analyzing, (checked_state > current_state) ? checked_state : current_state,
				 in_assignment, analyzed_variables);
	}
	if (rhs) {
	  rhs = analyzeVariables(BE->getRHS(), keep_analyzing, (checked_state > current_state) ? checked_state : current_state,
				 in_assignment, analyzed_variables);
	}
	// Now determine what to return
	int newstate = 0;
	if (BE->isMultiplicativeOp() || BE->isShiftOp()) {
	  if (lhs == LOCALLY_CONSTANT && rhs == LOCALLY_CONSTANT) {
	    newstate = LOCALLY_CONSTANT;
	  } else if ((lhs <= STRIDED && rhs == LOCALLY_CONSTANT)
		     || (rhs <= STRIDED && lhs == LOCALLY_CONSTANT)){
	    newstate = STRIDED;
	  } else {
	    newstate = UNSURE;
	  }
	} else {
	  if (lhs == LOCALLY_CONSTANT && rhs == LOCALLY_CONSTANT) {
	    newstate = LOCALLY_CONSTANT;
	  } else if (lhs < UNSURE && rhs < UNSURE) {
	    newstate = STRIDED;
	  } else {
	    newstate = UNSURE;
	  }
	}
	return (newstate > current_state) ? newstate : current_state;
      }
    }

    // Now handle unary operator specifics
    if (UnaryOperator *UE = dyn_cast<UnaryOperator>(S)) {
      std::string variable = assignsToVariable(UE, analyzed_variables);
      // Is this a variable we care about yet?
      if (analyzed_variables->find(variable) != analyzed_variables->end()) {
	// Alright, the variable modified by this unary operator is one we care about
	// Check if the unary operator is safe.
	if (checked_state != 2) {
	  // This is a safe operation, check to see if we are already 1
	  *keep_analyzing = (*analyzed_variables)[variable] != 1;
	  (*analyzed_variables)[variable] = 1;
	  // We can now exit this leaf for now. Return safe
	  return (checked_state > current_state) ? checked_state : current_state;
	} else {
	  // We are NOT safe
	  *keep_analyzing = (*analyzed_variables)[variable] != 2;
	  (*analyzed_variables)[variable] = 2;
	  return (checked_state > current_state) ? checked_state : current_state;
	}
      }
      // Fell through, we don't care so continue "regular" analysis.
    }

    

    if (S->child_begin() != S->child_end()) {
      // We have children. Recursively go down.
      for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
	int newstate = analyzeVariables(*child, keep_analyzing, (checked_state > current_state) ? checked_state : current_state,
					in_assignment, analyzed_variables);
	checked_state = (newstate > checked_state) ? newstate : checked_state;
      }
    }
    if (in_assignment)
      return (checked_state > current_state) ? checked_state : current_state;
    else
      return current_state;
  }
  
  void printVariableExpressions(Stmt* S, std::string check_variable,
			std::map<std::string, int>* analyzed_variables) {
    // Recursively go down on children.
    if (S == NULL) {
      return;
    }
    // Handle binary operation specifics
    if (BinaryOperator *BE = dyn_cast<BinaryOperator>(S)) {
      if (BE->isAssignmentOp() || BE->isCompoundAssignmentOp()) {
	// Check to see if we care about this assignment (namely, does it assign to a variable we care about)
	std::string variable = assignsToVariable(BE->getLHS(), analyzed_variables);
	if (variable == check_variable) {
	  // An assignment to the variable we care about
	  auto loopRange = CharSourceRange::getTokenRange(BE->getBeginLoc(), BE->getEndLoc());
	  auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	  //llvm::errs() << "Line: " << BE->getBeginLoc().getLine() << " Column: " << BE->getBeginLoc().getColumn() << ref.str() << "\n";
	  llvm::errs() << ref.str() << "\n";
	}
      }
    }

    // Now handle unary operator specifics
    if (UnaryOperator *UE = dyn_cast<UnaryOperator>(S)) {
      std::string variable = assignsToVariable(UE, analyzed_variables);
      // Is this a variable we care about yet?
      if (variable == check_variable) {
	// An assignment to the variable we care about
	auto loopRange = CharSourceRange::getTokenRange(UE->getBeginLoc(), UE->getEndLoc());
	auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	//llvm::errs() << "Line: " << UE->getBeginLoc().getLine() << " Column: " << UE->getBeginLoc().getColumn() << ref.str() << "\n";
	llvm::errs() << ref.str() << "\n";
	}
    }

    if (S->child_begin() != S->child_end()) {
      // We have children. Recursively go down.
      for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
	printVariableExpressions(*child, check_variable, analyzed_variables);
      }
    }
  }

  void findArrayUses(Stmt* S,
		     std::vector<ArraySubscriptExpr*>* local_array_accesses) {
    if (S == NULL) {
      return;
    }
    if (ArraySubscriptExpr *ASE = dyn_cast<ArraySubscriptExpr>(S)) {
      // We have an array access!
      local_array_accesses->push_back(ASE);
    }

    if (S->child_begin() != S->child_end()) {
      // We have children. Recursively go down.
      for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
	findArrayUses(*child, local_array_accesses);
      }
    }
  }

  void findIndexVariables(Stmt* S, std::string full_name,
			  std::map<std::string, std::vector<std::string>*>* array_index_variables,
			  std::map<std::string, int>* analyzed_variables)
  {
    if (DeclRefExpr* idx_ref = dyn_cast<DeclRefExpr>(S)) {
      auto loopRange = CharSourceRange::getTokenRange(idx_ref->getBeginLoc(), idx_ref->getEndLoc());
      auto ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
      //llvm::errs() << "Attempting to push back " << ref.str() << " onto " << full_name << "\n";
      (*array_index_variables)[full_name]->push_back(ref.str());
      //llvm::errs() << "Done attempting to push back " << ref.str() << " onto " << full_name << "\n";
      (*analyzed_variables)[ref.str()] = 0;
      return;
    }

    for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
      findIndexVariables(*child, full_name, array_index_variables, analyzed_variables);
    }
  }
	
  void handleFindArrayUses(Stmt* S,
		     std::vector<ArraySubscriptExpr*>* local_array_accesses) {
    if (ForStmt *FS = dyn_cast<ForStmt>(S)) {
      if (FS->child_begin() != FS->child_end()) {
	// We have children. Recursively go down.
	for (StmtIterator child = FS->child_begin(), child_end = FS->child_end(); child != child_end; ++child) {
	  if (*child != FS->getInit()) {
	    findArrayUses(*child, local_array_accesses);
	  }
	}
      }      
    }
    if (WhileStmt *WS = dyn_cast<WhileStmt>(S)) {
      if (WS->child_begin() != WS->child_end()) {
	// We have children. Recursively go down.
	for (StmtIterator child = WS->child_begin(), child_end = WS->child_end(); child != child_end; ++child) {
	  findArrayUses(*child, local_array_accesses);
	}
      }      
    }
  }
  
  void handlePrintVariableExpressions(Stmt* S, std::string check_variable,
				      std::map<std::string, int>* analyzed_variables) {
    if (ForStmt *FS = dyn_cast<ForStmt>(S)) {
      if (FS->child_begin() != FS->child_end()) {
	// We have children. Recursively go down.
	for (StmtIterator child = FS->child_begin(), child_end = FS->child_end(); child != child_end; ++child) {
	  if (*child != FS->getInit()) {
	    printVariableExpressions(*child, check_variable, analyzed_variables);
	  }
	}
      }      
    }
    if (WhileStmt *WS = dyn_cast<WhileStmt>(S)) {
      if (WS->child_begin() != WS->child_end()) {
	// We have children. Recursively go down.
	for (StmtIterator child = WS->child_begin(), child_end = WS->child_end(); child != child_end; ++child) {
	  printVariableExpressions(*child, check_variable, analyzed_variables);
	}
      }      
    }
  }

  void handleAnalyzeVariables(Stmt* S, bool* keep_analyzing,
			std::map<std::string, int>* analyzed_variables) {
    if (ForStmt *FS = dyn_cast<ForStmt>(S)) {
      if (FS->child_begin() != FS->child_end()) {
	// We have children. Recursively go down.
	for (StmtIterator child = FS->child_begin(), child_end = FS->child_end(); child != child_end; ++child) {
	  if (*child != FS->getInit()) {
	    analyzeVariables(*child, keep_analyzing, 0, false, analyzed_variables);
	  }
	}
      }      
    }
    if (WhileStmt *WS = dyn_cast<WhileStmt>(S)) {
      if (WS->child_begin() != WS->child_end()) {
	// We have children. Recursively go down.
	for (StmtIterator child = WS->child_begin(), child_end = WS->child_end(); child != child_end; ++child) {
	  analyzeVariables(*child, keep_analyzing, 0, false, analyzed_variables);
	}
      }      
    }
  }
  
  bool HandleTopLevelDecl(DeclGroupRef DG) override {
    loops_analyzed = 0;
    array_accesses_analyzed = 0;
    total_locally_constant = 0;
    total_strided = 0;
    total_unsure = 0;
    for (DeclGroupRef::iterator i = DG.begin(), e = DG.end(); i != e; ++i) {
      const Decl *D = *i;
      if (Stmt *body = D->getBody()) {
	// We have a body for this declaration
	for (StmtIterator child = body->child_begin(), child_end = body->child_end(); child != child_end; ++child) {
	  if (dyn_cast<ForStmt>(*child) || dyn_cast<WhileStmt>(*child)) {
	    // We have a loop statement. Analyze
	    loops_analyzed++;
	    std::map<std::string, int> analyzed_variables;
	    std::map<std::string, std::vector<std::string>*> array_index_variables;
	    std::map<std::string, std::vector<ArraySubscriptExpr*>*> array_indexes;
	    std::vector<std::string> local_constant;
	    std::vector<ArraySubscriptExpr*> local_array_accesses;
	    bool keep_working = true;
	    llvm::StringRef ref;
	    clang::CharSourceRange loopRange;
	    loopRange = CharSourceRange::getTokenRange(child->getBeginLoc(), child->getEndLoc());
	    ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	    if (ref.str() == "")
	      continue;
	    llvm::errs() << "Analyzing loop...\n";
	    llvm::errs() << ref.str() << "\n";
	    // We need to not handle the 
	    handleFindArrayUses(*child, &local_array_accesses);
	    for (auto ase_iter = local_array_accesses.begin(), ase_end = local_array_accesses.end(); ase_iter != ase_end; ase_iter++) {
	      std::string full_name = "";
	      for (StmtIterator base_array_child = (*ase_iter)->getBase()->child_begin(),
		     base_child_end = (*ase_iter)->getBase()->child_end();
		   base_array_child != base_child_end; ++base_array_child) {
		full_name = handleArrayAccesses(*base_array_child);
	      }
	      if (full_name != "") {
		array_index_variables.emplace(full_name, new std::vector<std::string>());
		if (array_indexes.find(full_name) == array_indexes.end())
		  array_indexes.emplace(full_name, new std::vector<ArraySubscriptExpr*>());
		array_indexes.find(full_name)->second->push_back((*ase_iter));
		findIndexVariables((*ase_iter)->getIdx(), full_name, &array_index_variables, &analyzed_variables);
	      }
	    }
	    // We now should have all array references and the variables they depend on. Now we should begin analyzing uses of the variables
	    llvm::errs() << "Done gathering array variables, analyzing...\n";
	    while (keep_working) {
	      keep_working = false;
	      handleAnalyzeVariables(*child, &keep_working, &analyzed_variables); 
	    }
	    // Done analyzing all the variables and main working list. Now analyze index for final state.
	    for (auto map_iter = array_index_variables.begin(); map_iter != array_index_variables.end(); map_iter++) {
	      llvm::errs() << map_iter->first << " index variables: ";
	      for (auto idx_iter = map_iter->second->begin(); idx_iter != map_iter->second->end(); idx_iter++) {
		llvm::errs() << *idx_iter;
		switch (analyzed_variables[*idx_iter]) {
		case LOCALLY_CONSTANT:
		  llvm::errs() << " is locally constant.\n";
		  break;
		case STRIDED:
		  llvm::errs() << " has a consistent strided access pattern.\n";
		  break;
		case UNSURE:
		  llvm::errs() << " cannot be proven to have a strided access pattern.\n";
		  break;
		}
		llvm::errs() << "Expressions modifying " << *idx_iter << ":\n";
		handlePrintVariableExpressions(*child, *idx_iter, &analyzed_variables);
	      }
	      // Walk all index expressions
	      for (auto idx_expr = array_indexes.find(map_iter->first)->second->begin();
		   idx_expr != array_indexes.find(map_iter->first)->second->end(); idx_expr++) {
		ArraySubscriptExpr* ASE = *idx_expr;
		int state = analyzeVariables(ASE->getIdx(), &keep_working, LOCALLY_CONSTANT, true,
					     &analyzed_variables);
		
		loopRange = CharSourceRange::getTokenRange(ASE->getBeginLoc(), ASE->getEndLoc());
		ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
		array_accesses_analyzed++;
		switch (state) {
		case LOCALLY_CONSTANT:
		  llvm::errs() << ref.str() << " has a constant access pattern.\n";
		  total_locally_constant++;
		  break;
		case STRIDED:
		  llvm::errs() << ref.str() << " has a strided access pattern.\n";
		  total_strided++;
		  break;
		case UNSURE:
		  llvm::errs() << ref.str() << " has a pattern we are unsure of.\n";
		  total_unsure++;
		  break;
		}
	      }
	    }
	  }	  
	}
      }
    }
    llvm::errs() << "Total Loops Analyzed: " << loops_analyzed << "\n";
    llvm::errs() << "Total Array Accesses Analyzed: " << array_accesses_analyzed << "\n";
    llvm::errs() << "    Locally Constant Patterns: " << total_locally_constant << "/"  << array_accesses_analyzed << "\n";
    llvm::errs() << "             Strided Patterns: " << total_strided << "/"  << array_accesses_analyzed << "\n";
    llvm::errs() << "              Unsure Patterns: " << total_unsure << "/"  << array_accesses_analyzed << "\n";
    return true;
  }

  void HandleTranslationUnit(ASTContext& context) override {
    if (!Instance.getLangOpts().DelayedTemplateParsing)
      return;

    // This demonstrates how to force instantiation of some templates in
    // -fdelayed-template-parsing mode. (Note: Doing this unconditionally for
    // all templates is similar to not using -fdelayed-template-parsig in the
    // first place.)
    // The advantage of doing this in HandleTranslationUnit() is that all
    // codegen (when using -add-plugin) is completely finished and this can't
    // affect the compiler output.
    struct Visitor : public RecursiveASTVisitor<Visitor> {
      const std::set<std::string> &ParsedTemplates;
      Visitor(const std::set<std::string> &ParsedTemplates)
          : ParsedTemplates(ParsedTemplates) {}
      bool VisitFunctionDecl(FunctionDecl *FD) {
        if (FD->isLateTemplateParsed() &&
            ParsedTemplates.count(FD->getNameAsString()))
          LateParsedDecls.insert(FD);
        return true;
      }

      std::set<FunctionDecl*> LateParsedDecls;
    } v(ParsedTemplates);
    v.TraverseDecl(context.getTranslationUnitDecl());
    clang::Sema &sema = Instance.getSema();
    for (const FunctionDecl *FD : v.LateParsedDecls) {
      clang::LateParsedTemplate &LPT =
          *sema.LateParsedTemplateMap.find(FD)->second;
      sema.LateTemplateParser(sema.OpaqueParser, LPT);
      llvm::errs() << "late-parsed-decl: \"" << FD->getNameAsString() << "\"\n";
    }   
  }
};

class AnalyzeStreamsAction : public PluginASTAction {
  std::set<std::string> ParsedTemplates;
protected:
  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 llvm::StringRef) override {
    return llvm::make_unique<AnalyzeStreamsConsumer>(CI, ParsedTemplates);
  }

  bool ParseArgs(const CompilerInstance &CI,
                 const std::vector<std::string> &args) override {
    for (unsigned i = 0, e = args.size(); i != e; ++i) {
      llvm::errs() << "AnalyzeStreams arg = " << args[i] << "\n";

      // Example error handling.
      DiagnosticsEngine &D = CI.getDiagnostics();
      if (args[i] == "-an-error") {
        unsigned DiagID = D.getCustomDiagID(DiagnosticsEngine::Error,
                                            "invalid argument '%0'");
        D.Report(DiagID) << args[i];
        return false;
      } else if (args[i] == "-parse-template") {
        if (i + 1 >= e) {
          D.Report(D.getCustomDiagID(DiagnosticsEngine::Error,
                                     "missing -parse-template argument"));
          return false;
        }
        ++i;
        ParsedTemplates.insert(args[i]);
      }
    }
    if (!args.empty() && args[0] == "help")
      PrintHelp(llvm::errs());

    return true;
  }
  void PrintHelp(llvm::raw_ostream& ros) {
    ros << "Help for AnalyzeStreams plugin goes here\n";
  }

};

}

static FrontendPluginRegistry::Add<AnalyzeStreamsAction>
X("analyze-streams", "Analyze potential stream patterns");
