\documentclass[conference]{IEEEtran}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings}
\title{Program Analysis to Discover Strided Access Patterns}
\author{William Galliher}
\date{May 2019}
\begin{document}
\maketitle
\begin{abstract}
Memory accesses within loops are areas of particular interest for acceleration, with many microarchitecture tools like prefetchers and accelerators aiming to exploit strided access patterns to speed up programs. However, finding patterns is often difficult, and there is a lack of easy to use tools to assist programmers in this effort.

To help reduce this effort, we propose an analysis tool to assist in identifying candidate memory accesses and expressions that form the pattern. Using these tools will point the programmer towards useful candidate areas, and can accelerate transforming programs to exploit microarchitectural features.
\end{abstract}
\section{Introduction}
In the age of powerful multi-core processors and new accelerators, memory continues to be a main focus for many programs. Programs such as image processing, neural nets, database operations, scientific computing, and cryptocurrency often work over large data sets, incorporating loops that consume these datasets to do different operations.

To accelerate these programs, there are microarchitectural features and accelerators that can exploit strided access patterns to speed up memory access. These include memory prefetch engines that can either be hinted to prefetch from memory, or attempt to determine what to prefetch without any input from the programmer, and accelerators such as Stream Dataflow accelerators. Stream Dataflow accelerators are accelerators that can perform computation kernels that are fed all of their data using memory ``streams'', which describe a strided access pattern and are consumed by specialized hardware to access memory in a fast and efficient manner.

Consider the following dot product example:
\begin{lstlisting}[language=C++]
#include <stdio.h>
#include <inttypes.h>

#define ALEN 256

uint16_t array1[ALEN]={1};
uint16_t array2[ALEN]={1};

uint64_t out = 0;

int main(int argc, char* argv[]) {
  for(int i = 0; i < ALEN; ++i) {
    array1[i]=1;
    array2[i]=1;
  }

  uint16_t sum0=0;
  uint64_t sum1=0, sum2=0;

  for(int i = 0; i < ALEN; ++i) {
    sum0 += array1[i] * array2[i];
  }
  printf(``dot product (original): %d\n'',sum0);
}
\end{lstlisting}

This program has two loops, and in both loops, the two arrays (\verb|array1| and \verb|array2|), are accessed in a linear manner based on the loop variable \verb|i|, which increments by 1 every iteration of the loop. Such a pattern can be exploited to prefetch or accelerate these memory accesses.

However, knowing these patterns are good and rewriting existing programs to make use of these patterns are two different things. Taking an existing program and determining which memory accesses exhibit strided access patterns can often be a confusing and high effort task, particularly if the programmer is not experienced in looking for these patterns. To help with this, we propose a way of identifying memory accesses at compile time, that reports both different memory accesses inside of loops and details if the access fits a strided access pattern over all iterations of the loop, and what expressions in the loop are factored into that decision. We further implement this algorithm into a popular compiler tool, enabling a pattern report to be given at compile time to the programmer to jump start their efforts in optimizing their memory access.

\section{Pattern Discovery Algorithm}
\subsection{Basics and Assumptions}
When determining which patterns we are attempting to detect, we focused on strided access patterns, the pattern used in Stream Dataflow accelerators. Strided access patterns refer to a constant access size and stride size where the stride amount is either equal to the amount of iterations for the loop, or a high percentage of the iterations of the loop. Such strided access patterns are good candidates for programmers to exploit for accelerating memory accesses.

Strided access patterns themselves are patterns of memory access described using three numbers:
\begin{itemize}
\item \textbf{Access size:} This is the size of the memory access, generally given in bytes.
\item \textbf{Stride size:} This is the amount the address for the memory access is modified for the next access, generally given in bytes.
\item \textbf{Stride amount:} This is the amount of times a particular constant access size and stride size is repeated.
\end{itemize}

Therefore, to determine if a particular memory access fits a strided access pattern, we want to determine that, for every iteration of a loop, the access size is constant and the stride size is constant. If this holds for every iteration of the loop, then the stride amount is then the amount of iterations of the loop. Note, we do not need to know the actual value of the access size, stride size, or stride amount, we merely just need to determine if they are constant at the point in the program where we are executing the loop.


\section{Methodology}
We implemented our algorithm as a plugin in Clang to assist in identifying these patterns. We chose Clang because it is a robust compiler frontend for llvm and offers a full and robust library for writing abstract syntax tree traversers. Since the frontend supports a number of languages that will be converted to the internal abstract syntaxt tree representation, the same plugin can be used without extensive rewriting necessary for different language support. In addition, the Clang development team has written many examples and extensive documentation for using their abstract syntax tree representation, further reducing the effort of implementation for our plugin.

\end{document}
