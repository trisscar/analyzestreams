//===- PrintFunctionNames.cpp ---------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Example clang plugin which simply prints the names of all the top-level decls
// in the input file.
//
//===----------------------------------------------------------------------===//

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Sema/Sema.h"
#include "llvm/Support/raw_ostream.h"
#include <vector>
using namespace clang;

namespace {

class AnalyzeStreamsConsumer : public ASTConsumer {
  CompilerInstance &Instance;
  std::set<std::string> ParsedTemplates;

public:
  AnalyzeStreamsConsumer(CompilerInstance &Instance,
                         std::set<std::string> ParsedTemplates)
      : Instance(Instance), ParsedTemplates(ParsedTemplates) {}

  void findArrayUses(Stmt* S, bool* analyze_loop,
		     std::map<std::string, bool>* variable_strides,
		     std::vector<std::string>* local_constant,
		     std::vector<ArraySubscriptExpr*>* local_array_accesses) {
    if (S == NULL) {
      return;
    }
    if (ArraySubscriptExpr *ASE = dyn_cast<ArraySubscriptExpr>(S)) {
      // We have an array access!
      local_array_accesses->push_back(ASE);
    }

    if (S->child_begin() != S->child_end()) {
      // We have children. Recursively go down.
      for (StmtIterator child = S->child_begin(), child_end = S->child_end(); child != child_end; ++child) {
	findArrayUses(*child, analyze_loop, variable_strides, local_constant, local_array_accesses);
      }
    }
  }

  bool HandleTopLevelDecl(DeclGroupRef DG) override {
    for (DeclGroupRef::iterator i = DG.begin(), e = DG.end(); i != e; ++i) {
      const Decl *D = *i;
      if (Stmt *body = D->getBody()) {
	// We have a body for this declaration
	for (StmtIterator child = body->child_begin(), child_end = body->child_end(); child != child_end; ++child) {
	  if (dyn_cast<ForStmt>(*child) || dyn_cast<WhileStmt>(*child)) {
	    // We have a FOR statement. Analyze
	    std::map<std::string, bool> variable_strides;
	    std::map<std::string, std::vector<std::string>*> array_index_variables;
	    std::vector<std::string> local_constant;
	    std::vector<ArraySubscriptExpr*> local_array_accesses;
	    bool analyzeLoop = true;
	    llvm::errs() << "Analyzing loop..." << *child->getStmtClassName() << "\n";
	    CharSourceRange loopRange = CharSourceRange::getTokenRange(child->getBeginLoc(), child->getEndLoc());
	    llvm::StringRef ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	    llvm::errs() << ref.str() << "\n";
	    while (analyzeLoop) {
	      analyzeLoop = false;
	      findArrayUses(*child, &analyzeLoop, &variable_strides, &local_constant, &local_array_accesses);
	    }
	    for (auto ase_iter = local_array_accesses.begin(), ase_end = local_array_accesses.end(); ase_iter != ase_end; ase_iter++) {
	      DeclRefExpr *base_expr = NULL;
	      for (StmtIterator base_array_child = (*ase_iter)->getBase()->child_begin(),
		     base_child_end = (*ase_iter)->getBase()->child_end();
		   base_array_child != base_child_end; ++base_array_child) {
		if (base_expr && dyn_cast<DeclRefExpr>(*base_array_child)) {
		  llvm::errs() << "Multi decl ref found\n";
		  continue;
		}
		if (dyn_cast<DeclRefExpr>(*base_array_child)) {
		  base_expr = dyn_cast<DeclRefExpr>(*base_array_child);
		  loopRange = CharSourceRange::getTokenRange(base_expr->getBeginLoc(), base_expr->getEndLoc());
		  ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
		  array_index_variables.emplace(ref.str(), new std::vector<std::string>());
		}
	      }
	      loopRange = CharSourceRange::getTokenRange(base_expr->getBeginLoc(), base_expr->getEndLoc());
	      llvm::StringRef baseref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
	      for (StmtIterator idx_array_child = (*ase_iter)->getIdx()->child_begin(),
		     idx_child_end = (*ase_iter)->getIdx()->child_end();
		   idx_array_child != idx_child_end; ++idx_array_child) {
		if (DeclRefExpr* idx_ref = dyn_cast<DeclRefExpr>(*idx_array_child)) {
		  loopRange = CharSourceRange::getTokenRange(idx_ref->getBeginLoc(), idx_ref->getEndLoc());
		  ref = Lexer::getSourceText(loopRange, Instance.getASTContext().getSourceManager(), LangOptions());
		  array_index_variables[baseref.str()]->push_back(ref.str());
		}
	      }
	    }
	    for (auto map_iter = array_index_variables.begin(); map_iter != array_index_variables.end(); map_iter++) {
	      llvm::errs() << map_iter->first << " index variables: ";
	      for (auto idx_iter = map_iter->second->begin(); idx_iter != map_iter->second->end(); idx_iter++) {
		llvm::errs() << *idx_iter << " ";
	      }
	      llvm::errs() << "\n";
	    }
	  }	  
	}
      }
    }

    return true;
  }

  void HandleTranslationUnit(ASTContext& context) override {
    if (!Instance.getLangOpts().DelayedTemplateParsing)
      return;

    // This demonstrates how to force instantiation of some templates in
    // -fdelayed-template-parsing mode. (Note: Doing this unconditionally for
    // all templates is similar to not using -fdelayed-template-parsig in the
    // first place.)
    // The advantage of doing this in HandleTranslationUnit() is that all
    // codegen (when using -add-plugin) is completely finished and this can't
    // affect the compiler output.
    struct Visitor : public RecursiveASTVisitor<Visitor> {
      const std::set<std::string> &ParsedTemplates;
      Visitor(const std::set<std::string> &ParsedTemplates)
          : ParsedTemplates(ParsedTemplates) {}
      bool VisitFunctionDecl(FunctionDecl *FD) {
        if (FD->isLateTemplateParsed() &&
            ParsedTemplates.count(FD->getNameAsString()))
          LateParsedDecls.insert(FD);
        return true;
      }

      std::set<FunctionDecl*> LateParsedDecls;
    } v(ParsedTemplates);
    v.TraverseDecl(context.getTranslationUnitDecl());
    clang::Sema &sema = Instance.getSema();
    for (const FunctionDecl *FD : v.LateParsedDecls) {
      clang::LateParsedTemplate &LPT =
          *sema.LateParsedTemplateMap.find(FD)->second;
      sema.LateTemplateParser(sema.OpaqueParser, LPT);
      llvm::errs() << "late-parsed-decl: \"" << FD->getNameAsString() << "\"\n";
    }   
  }
};

class AnalyzeStreamsAction : public PluginASTAction {
  std::set<std::string> ParsedTemplates;
protected:
  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 llvm::StringRef) override {
    return llvm::make_unique<AnalyzeStreamsConsumer>(CI, ParsedTemplates);
  }

  bool ParseArgs(const CompilerInstance &CI,
                 const std::vector<std::string> &args) override {
    for (unsigned i = 0, e = args.size(); i != e; ++i) {
      llvm::errs() << "AnalyzeStreams arg = " << args[i] << "\n";

      // Example error handling.
      DiagnosticsEngine &D = CI.getDiagnostics();
      if (args[i] == "-an-error") {
        unsigned DiagID = D.getCustomDiagID(DiagnosticsEngine::Error,
                                            "invalid argument '%0'");
        D.Report(DiagID) << args[i];
        return false;
      } else if (args[i] == "-parse-template") {
        if (i + 1 >= e) {
          D.Report(D.getCustomDiagID(DiagnosticsEngine::Error,
                                     "missing -parse-template argument"));
          return false;
        }
        ++i;
        ParsedTemplates.insert(args[i]);
      }
    }
    if (!args.empty() && args[0] == "help")
      PrintHelp(llvm::errs());

    return true;
  }
  void PrintHelp(llvm::raw_ostream& ros) {
    ros << "Help for AnalyzeStreams plugin goes here\n";
  }

};

}

static FrontendPluginRegistry::Add<AnalyzeStreamsAction>
X("analyze-streams", "Analyze potential stream patterns");
